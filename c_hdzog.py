#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scraper import scraper_content

class scraper(scraper_content):

  def get_prevs(self, soup):
    return soup.find('div', {'class':'thumbs-videos'}).findAll('a')

  def scrap_video(self, post):
    url = post['href']
    thumb = post.find('img')['src']
    title = post['title']
    
    soup = self.get_soup(url)
    
    tags = list(soup.findAll('li', {'class':'tags'})) + list(soup.findAll('li', {'class':'tags'}))
    tags =[li.findAll('a') for li in tags]
    tags = [[a.text for a in aas] for aas in tags]
    tags = list(sum(tags, []))

    lenght = post.find('span', {'class':'time'}).text
    lenght = ([0]+[int(l) for l in lenght.split(':')])[::-1]
    lenght = lenght[0] + lenght[1]*60 + lenght[2]*60

    embed = url.split('/')[4]
    video_link = url
    return (locals())


scraper(f=__file__,
        site = 23,
        url = 'http://www.hdzog.com/popular/{0}/',
        page = 1,
        last_page = 600,
        id_num = 23000000,
        flag = 100)()
