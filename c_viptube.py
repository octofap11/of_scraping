#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scraper import scraper_content

class scraper(scraper_content):

  def get_prevs(self, soup):
    return soup.findAll('a', {'class':'th related_'})

  def scrap_video(self, post):
    url = self.get_link(post['href'])
    title = post['title']
    thumb = post.find('img')['src']
    
    soup = self.get_soup(url)
    
    tags = soup.find('div', {'class':'data_categories'}).findAll('a')
    tags = [tag['title'] for tag in tags]

    lenght = post.find('i', {'class':'time2'}).text
    lenght = ([0]+[int(l) for l in lenght.split(':')])[::-1]
    lenght = lenght[0] + lenght[1]*60 + lenght[2]*60

    embed = url.split('/')[4]
    video_link = url
    return (locals())


scraper(f=__file__,
        site = 19,
        url = 'http://www.viptube.com/videos/{0}',
        page = 1,
        last_page = 11600,
        id_num = 19000000,
        flag = 100)()
