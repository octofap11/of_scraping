#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scraper import scraper_content
import re

class scraper(scraper_content):

  def get_prevs(self, soup):
    return soup.findAll('div', {'class':'thumb_box'})

  def scrap_video(self, post):
    thumb = post.find('img', {'class': 'videoThumbs'})
    title=thumb['title']
    try:
      thumb = thumb['data-thumb']
    except KeyError:
      thumb = thumb['src']
    video_link = post.find('a')
    video_link = video_link['href']
    embed = video_link.split('/')[-2]
    lenght_ = post.find('div', {'class':'video_duration'}).text.split(':')[::-1]
    lenght = int(lenght_[0]) + int(lenght_[1])*60
    if len(lenght_) == 3:
      lenght += int(lenght_[2])*3600
    video_soup = self.get_soup(video_link)
    tags = [tag.text for tag in video_soup.find('li', {'class':'tag-list'}).findAll('a')]
    tags += [video_soup.find('div', {'id':'vidInfoCol_01'}).findAll('li')[1].find('a').text]
    tags = [tag for tag in tags if tag != '']

    return (locals())


scraper(f=__file__,
        site = 4,
        url = 'http://www.tube8.com/latest/page/{0}/',
        page = 0,
        last_page = 7910,
        id_num = 4000000,
        flag = 100)()
