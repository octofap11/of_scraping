#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scraper import scraper_content

class scraper(scraper_content):

  def get_prevs(self, soup):
    return soup.find('ul', {'class':'section03 float-left videoList'}).\
        findAll('li', {'class':'float-left'})

  def scrap_video(self, post):
    thumb = post.find('img')['data-srcmedium']
    title = post.find('img')['alt']
    url = post.find('a')['href']

    soup = self.get_soup(url)

    tags = soup.findAll('div', {'class':'ibData'})
    tags =[div.findAll('a') for div in tags]
    tags = [[a.text for a in aas] for aas in tags]
    tags = list(sum(tags, []))

    lenght = post.find('div', {'class':'videoDuration'}).find('div').text
    lenght = [int(i) for i in lenght.split(':')][::-1]
    lenght_ = 0
    if len(lenght) == 3:
      lenght_ += lenght[2]*60*60
    lenght_ += lenght[1]*60 + lenght[0]
    lenght = lenght_

    embed = url.split('/')[-1] 
    video_link = url
    return (locals())


scraper(f=__file__,
        site = 17,
        url = 'http://www.extremetube.com/videos?page={0}',
        page = 2,
        last_page = 1550,
        id_num = 17000000,
        flag = 100)()
