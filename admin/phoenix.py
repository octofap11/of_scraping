# -*- coding: utf-8 -*-
"""
    Flaskr
    ~~~~~~

    A microblog example application written as Flask tutorial with
    Flask and sqlite3.

    :copyright: (c) 2010 by Armin Ronacher.
    :license: BSD, see LICENSE for more details.
"""

import random

from flask import Flask, request, session, redirect, url_for, \
     render_template, flash

from of_engine.scraper_manager import Scrapers

# configuration
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'default'
SCRAPERS_PATH = '/home/lander/repos/of_scraping'
# create our little application :)
app = Flask(__name__)
app.config.from_object(__name__)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

scrapers = Scrapers( SCRAPERS_PATH, db=0)

@app.route('/')
def show_entries():
    global scrapers
    if 'logged_in' in session and session['logged_in']:
        scrapers.fetch_all()
        scrapers_all = scrapers.all
    else:
        scrapers_all = []
    return render_template('show_entries.html', scrapers=scrapers_all)


@app.route('/order')
def order():
    if 'logged_in' in session and session['logged_in']:
        scraper = scrapers.get(
            request.args['scraper']
        )

        scraper.set_order(
            request.args['order']
        )
        return 'OK'
    else:
        return 'You need to log in'

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))


if __name__ == '__main__':
    app.run()
