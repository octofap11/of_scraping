
function order(caller) {
    var scraper = $(caller).data('scraper');
    var order = $(caller).text().toLowerCase();
    console.log(scraper+' -> order = '+order);
    $.get('/order?scraper='+scraper+'&order='+order);
    Refresh();
}
function porder(caller) {
    var scraper = $(caller).data('scraper');
    console.log(scraper + ' cancel order');
    $.get('/order?scraper=' + scraper + '&order');
    Refresh();
}

$( document ).ready(function () {
    setInterval(Refresh, 2000);
});


function Refresh() {
    $.get('/', function(result) {
        var new_scrapers = $('<div></div>').append(result).find('.scrapers').html();
        $('.scrapers').html(new_scrapers);
        $('.scrapers').toggleClass('offline', false );
    }).fail(function() {
        $('.scrapers').toggleClass('offline', true );
  });
}