#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scraper import scraper_content

class scraper(scraper_content):

  def get_prevs(self, soup):
    return soup.find('div', {'class':'thumbs'}).findAll('a')

  def scrap_video(self, post):
    url = post['href']
    title = post.find('p').text
    thumb = post.find('img')['data-original']
    
    soup = self.get_soup(url)
    
    tags = soup.find('div', {'class':'tags'}).findAll('a')
    tags = [tag.text for tag in tags]

    lenght = -1#soup.find('div', {'class':'video-info'}).text

    embed = url.split('/')[4]
    video_link = url
    return (locals())


scraper(f=__file__,
        site = 18,
        url = 'http://www.faptube.com/latest-updates/{0}/',
        page = 1,
        last_page = 670,
        id_num = 18000000,
        flag = 100)()
