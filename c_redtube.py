#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scraper import scraper_content


class scraper(scraper_content):
    def get_prevs(self, soup):
        return soup.find('div', {'class': 'videosTable'}).findAll('li')

    def scrap_video(self, post):
        a = post.find('a')
        try:
            try:
                thumb = a.find('img')['data-src']
            except:
                thumb = a.find('img')['src']
        except AttributeError:
            raise RuntimeWarning()
        title = a.find('img')['title']
        url = self.get_link(a['href'])
        soup = self.get_soup(url)
        tds = [td.findAll('a') for td in soup.findAll('td', {'class': 'links'})]
        tags = []
        for td in tds:
            for a in td:
                if a.text != '+':
                    tags += [a.text]
        try:
            lenght = post.find('span', {'class': 'video-duration'}).text
        except AttributeError:
            raise RuntimeWarning()
        lenght = [int(l) for l in lenght.split(':')][::-1]
        lenght_ = 0
        if len(lenght) == 3:
            lenght_ += lenght[2] * 60 * 60
        lenght_ += lenght[1] * 60 + lenght[0]
        lenght = lenght_

        embed = url.split('/')[-1]
        video_link = url
        return (locals())


scraper(f=__file__,
        site=8,
        url='http://www.redtube.com/?page={0}',
        page=1,
        last_page=2530,
        id_num=8000000,
        flag=100)()
