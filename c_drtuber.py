#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scraper import scraper_content

class scraper(scraper_content):

  def get_prevs(self, soup):
    return soup.findAll('a', {'class':'th ch-video'})

  def scrap_video(self, post):
    thumb = post.find('img')['src'].split('?')[0]
    title = post.find('img')['alt']
    url = self.get_link(post['href'])

    soup = self.get_soup(url)

    tags = soup.find('div', {'class':'categories_list'})
    tags =list(tags.findAll('a'))
    tags = [tag.text for tag in tags]

    lenght = post.find('em', {'class':'time_thumb'})
    lenght = [int(i) for i in lenght.text.split(':')][::-1]
    lenght_ = 0
    if len(lenght) == 3:
      lenght_ += lenght[2]*60*60
    lenght_ += lenght[1]*60 + lenght[0]
    lenght = lenght_

    embed = url.split('/')[-2]
    video_link = url
    return (locals())


scraper(f=__file__,
        site = 15,
        url = 'http://www.drtuber.com/videos/{0}',
        page = 1,
        last_page = 2300,
        id_num = 15000000,
        flag = 100)()
