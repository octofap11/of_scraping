#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scraper import scraper_content

class scraper(scraper_content):

  def get_prevs(self, soup):
    return soup.findAll('span', {'id':'miniatura'})

  def scrap_video(self, post):
    thumb = post.find('img', {'class':'lazy'})['data-original']
    url = post.find('a', {'class':'frame'})['href']
    url = self.get_link(url)
    embed = url.split('.')[-2].split('-')[-1]
    lenght_ = post.find('span', {'class':'thumbtime'}).text.split(':')[::-1]
    lenght = int(lenght_[0]) + int(lenght_[1])*60
    if len(lenght_) == 3:
      lenght += int(lenght_[2])*3600
    soup = self.get_soup(url)
    title = soup.find('div', {'id':'video_text'}).find('h3').text
    tags = soup.find('div', {'id':'tags1'}).findAll('a')
    tags = [tag.text for tag in tags[1:]]
    video_link = url
    return (locals())


scraper(f=__file__,
        site = 5,
        url = 'http://www.youjizz.com/page/{0}.html',
        page = 1,
        last_page = 24200,
        id_num = 5000000,
        flag = 100)()
