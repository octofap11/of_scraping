#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scraper import scraper_content

class scraper(scraper_content):

  def get_prevs(self, soup):
    return soup.find('div', {'class':'thumb_holder'}).findAll('a')

  def scrap_video(self, post):
    url = post['href']
    thumb = post.find('img')['src']
    title = post.find('img')['alt']
    
    soup = self.get_soup(url)
    
    tags = list(soup.findAll('p', {'class':'info_tags'})) + list(soup.findAll('p', {'class':'info_cats'}))
    tags =[li.findAll('a') for li in tags]
    tags = [[a.text for a in aas] for aas in tags]
    tags = list(sum(tags, []))

    lenght = post.find('span', {'class':'dur'}).text
    lenght = ([0]+[int(l) for l in lenght.split(':')])[::-1]
    lenght = lenght[0] + lenght[1]*60 + lenght[2]*60

    embed = thumb.split('/')[6]
    video_link = url
    return (locals())


scraper(f=__file__,
        site = 24,
        url = 'http://www.privatehomeclips.com/latest-updates/{0}/',
        page = 1,
        last_page = 1840,
        id_num = 24000000,
        flag = 100)()
