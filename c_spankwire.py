#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scraper import scraper_content
import re

class scraper(scraper_content):

  def get_prevs(self, soup):
    return soup.findAll('div', {'class':'thumb-info-wrapper'})

  def scrap_video(self, post):
    title = post.find('div', {'class':'title-video'}).find('a')
    url = self.get_link(title['href'])
    title = title.text
    thumb = post.find('img')
    thumb = thumb['data-original']
    embed = url.split('video')[-1][:-1]
    lenght_ = post.find('div', {'class':'info-box'})
    lenght_ = lenght_.text.replace(lenght_.find('span').text, '')
    lenght_ = lenght_.split(':')[::-1]
    lenght = int(lenght_[0]) + int(lenght_[1])*60
    if len(lenght_) == 3:
      lenght += int(lenght_[2])*3600
    views = post.find('div', {'class':'views-box'}).text.replace('views', '').replace(',','')
    views = int(views)/10
    soup = self.get_soup(url)
    tags = soup.find('div', {'class':re.compile('^video-info-tag')}).findAll('a')
    tags = [tag.text for tag in tags[1:]]
    tags += [cat.text for cat in soup.findAll('a', {'id':'categoryLink'})]
    tags = tags[::-1]
    video_link = url
    return (locals())


scraper(f=__file__,
        site = 6,
        url = 'http://www.spankwire.com/home2/Straight/Featured/All_Time/Submitted?Page={0}',
        page = 1,
        last_page = 6450,
        id_num = 6000000,
        flag = 100)()
