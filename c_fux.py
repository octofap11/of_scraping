#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scraper import scraper_content

class scraper(scraper_content):

  def get_prevs(self, soup):
    return soup.findAll('div', {'class':'col thumb_video'})

  def scrap_video(self, post):
    a = post.find('a')
    thumb = a.find('img')['data-master']
    title = a['title']
    url = self.get_link(a['href'])
    soup = self.get_soup(url)
    try:
      tags_ = list(soup.find('div', {'class':'user'}).findAll('a'))
    except:
      tags_ = []
    tags_ += list(soup.find('div', {'class':'tags'}).findAll('a'))
    tags = []
    for tag in tags_:
      try:
        tags.append(tag['title'])
      except:
        tags.append(tag.find('img')['alt'])
    lenght = post.find('li', {'class':'duration-top'}).text
    lenght = [int(l) for l in lenght.split(':')][::-1]
    lenght_ = 0
    if len(lenght) == 3:
      lenght_ += lenght[2]*60*60
    lenght_ += lenght[1]*60 + lenght[0]
    lenght = lenght_

    embed = url.split('/')[-2]
    video_link = url
    return (locals())


scraper(f=__file__,
        site = 12,
        url = 'http://www.fux.com/video?p={0}',
        page = 1,
        last_page = 5300,
        id_num = 12000000,
        flag = 100)()
