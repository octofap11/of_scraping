#!/usr/bin/env python
# -*- coding: utf-8 -*-
# REDIRECTS TO http://www.txxx.com/, explore network!
from scraper import scraper_content

class scraper(scraper_content):

  def get_prevs(self, soup):
    return soup.find('div', {'class':'thumb_holder'}).findAll('a', {'class':'thumb'})

  def scrap_video(self, post):
    url = post['href']
    thumb = post.find('img')
    title = thumb['alt']
    thumb = thumb['src']
    
    soup = self.get_soup(url)
    
    tags = soup.find('div', {'class':'tools'}).findAll('a')
    tags = [tag.text for tag in tags]

    lenght = post.find('span', {'class':'dur'}).text
    lenght = ([0]+[int(l) for l in lenght.split(':')])[::-1]
    lenght = lenght[0] + lenght[1]*60 + lenght[2]*60

    embed = url.split('/')[4]
    video_link = url
    return (locals())


scraper(f=__file__,
        site = 22,
        url = 'http://www.tubecup.com/latest-updates/{0}/',
        page = 1,
        last_page = 4300,
        id_num = 22000000,
        flag = 100)()
