#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import socket

from threading import Thread
import httplib
import urllib2
from of_engine import main as client
from of_engine import conf
from of_engine.scraper_manager import ScraperManager, UnloadException
import time

class scraper_content:
    def __init__(self, f, site, url, page, last_page, id_num, flag=0):
        self.f = f
        self.site = site
        self.url = url
        self.cookie = ''
        self.id_num = id_num
        self.flag = flag
        self.of = client.of(conf)
        self.conf = conf

        self.name = f.split('/')[-1][2:-3]
        self.sm = ScraperManager(self.name, instance=self)

    def __call__(self):
        try:
            while True:
                if self.sm.run():
                    self.sm.update()
                    self.page = self.sm.page
                    self.scrap_page()
                    self.sm.next_page()
                else:
                    time.sleep(1)
        except KeyboardInterrupt:
            exit()
        except UnloadException:
            exit()

    def get_prevs(self, soup):
        '''Should receive the soup of a page, and return all the previews divs'''
        pass

    def scrap_page(self):
        page_url = self.url.replace('{0}', str(self.page))
        self.dom = self.get_dom(page_url)

        try:
            page_soup = self.get_soup(page_url)
            prevs_soups = self.get_prevs(page_soup)
        except:
            page_soup = self.get_soup(page_url)
            prevs_soups = self.get_prevs(page_soup)

        self.t_pool = []
        for prev_soup in prevs_soups:
            try:
                t = Thread(target=self.scrap_upsert_video, args=(prev_soup,))
                self.t_pool.append(t)
            except UnicodeEncodeError:
                print "UnicodeEncodeError"

        for t in self.t_pool:
            try:
                time.sleep(0.02)
                t.start()
            except KeyboardInterrupt:
                time.sleep(0.02)
                t.start()

        for t in self.t_pool:
            try:
                t.join()
            except KeyboardInterrupt:
                t.join()

    def scrap_upsert_video(self, prev_soup):
        try:
            l = self.scrap_video(prev_soup)
        except RuntimeWarning:
            return
        except:
            print("ERROR!")
        self.upsert_video(
            url=l['video_link'],
            title=l['title'],
            thumb=l['thumb'],
            embed=l['embed'],
            lenght=l['lenght'],
            tags=l['tags'],
        )

    def scrap_video(self, prev_soup):
        '''Recives a preview soup of a video, and should call upsert_video to index it'''
        pass

    def upsert_video(self, url, title, thumb, embed, lenght, tags):
        new = self.of.upsert_video(
            id_num=self.sm.id(embed),
            site=self.site,
            url=url,
            title=title,
            thumb=thumb,
            embed=embed,
            lenght=lenght,
            tags=tags,
            flag=self.flag,
        )
        self.sm.increse_nvideos()

    def get_link(self, link):
        if link.startswith('/'):
            link = 'http://' + self.dom + link
        elif not link.startswith('http'):
            link = 'http://' + self.dom + '/' + link
        return link.encode('utf-8', 'ignore')

    def get_dom(self, url):
        url = url[7:]
        url = url[:url.find('/')]
        return url

    def get_soup(self, url, n=0):
        try:
            if n > 10:
                raise Exception('Not trying again!')
            return self.get_soup_base(url)
        except socket.timeout:
            print 'TIMED OUT!!! n=', n, 'url=', url
            return self.get_soup(url, n + 1)
        except urllib2.URLError as e:
            print e.reason, '!!! n=', n, 'url=', url
        except httplib.BadStatusLine:
            print 'BAD STATUS LINE!!! n=', n, 'url=', url

    def get_soup_base(self, url):
        req = urllib2.Request(url, headers={'User-Agent': 'Mozilla/5.0', 'Cookie': self.cookie})
        html = urllib2.urlopen(req, timeout=30).read()
        html = unicode(html.decode('utf-8', 'ignore'))
        soup = BeautifulSoup(html)
        return soup

class scraper_searches(scraper_content):
    def get_searches(self, soup):
        '''Should receive the soup of a page, and return all the searches array'''
        pass

    def scrap_page(self):
        page_url = self.url.replace('{0}', str(self.page))
        self.dom = self.get_dom(page_url)

        page_soup = self.get_soup(page_url)
        searches = self.get_searches(page_soup)
        for s in searches:
            self.of.upsert_query(s, self.id_num)
            self.id_num += 1
